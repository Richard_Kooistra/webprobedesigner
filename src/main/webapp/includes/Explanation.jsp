<%--
  Created by IntelliJ IDEA.
  User: rkooistra
  Date: 18-3-18
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div>
    <b>Summary</b> : This will create a detailed summary of the chosen file.<br />
    <b>Design</b> : This will generate a probe using the default settings.<br />
    <b>Minimum probe length</b> : This will create a probe of at least the length given.<br />
    <b>Maximum probe length</b> : This will create a probe no longer then length given.<br />
    <b>Maximum number of probes</b> : This will create a number of probes based on the number given.<br />
</div>
