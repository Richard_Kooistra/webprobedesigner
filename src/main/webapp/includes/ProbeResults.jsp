<%--
  Created by IntelliJ IDEA.
  User: rkooistra
  Date: 19-4-18
  Time: 21:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="probeResults">

    <div class="row">
        <div class="col-md-4">
            <div id="Design">
                <b>The default Desgin option resulted in the following probe:</b><br />
                <table class="table table-sm table-responsive">
                    <tbody>
                    <tr>
                        <th scope="row">Start</th>
                        <td>${requestScope.designStart}</td>
                    </tr>
                    <tr>
                        <th scope="row">Length</th>
                        <td>${requestScope.designLength}</td>
                    </tr>
                    <tr>
                        <th scope="row">Sequence</th>
                        <td>${requestScope.designSequence}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <hr />
            <div id="MinimunProbeLength">
                <b>The Minimum Probe Length option resulted in the following probe: </b><br />
                ${requestScope.MinimumProbeLength}
            </div>
            <hr />
            <div id="MaximumProbeLength">
                <b>The Maximum Probe Length option resulted in the following probe: </b><br />
                ${requestScope.MaximumProbeLength}
            </div>
            <hr />
        </div>

        <div class="col-md-4">
            <div id="NumberOfProbes">
                <b>The Number of Probes options resulted in the following probes:</b><br />
                <c:forEach var="probe" items="${requestScope.NumberOfProbes}">
                    <tr>
                        <td>${probe}</td><br />
                    </tr>
                </c:forEach>
            </div>
            <hr />
        </div>

        <div class="col-md-4">
            <div id="Summary">
                <b>The Summary option resulted in the following file overview:</b><br />
                <c:forEach var="probe" items="${requestScope.msaFileSummary}">
                    <table class="table table-sm table-responsive">
                        <tbody>
                        <tr>
                            <th scope="row">${probe.key} = </th>
                            <td>${probe.value}</td>
                        </tr>
                        </tbody>
                    </table>
                </c:forEach>
            </div>
        </div>
    </div>

</div>

