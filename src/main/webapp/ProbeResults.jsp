<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: rkooistra
  Date: 19-3-18
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <title>Probe Results</title>
</head>
<body>

    <div class="container">

        <div>
            <p>
                <jsp:include page="includes/Banner.jsp"></jsp:include>
            </p>
        </div>

        <div>
            <p>
            <h1>
                Probe Results
            </h1>
        </div>

        <div>
            <p>
            <hr />
            <b>The minimum length chosen</b>   :   ${requestScope.minimum}<br />
            <b>The maximum length chosen</b>   :   ${requestScope.maximum}<br />
            <b>The number of probes chosen</b> :   ${requestScope.number}<br />
            <b>The summary has been checked</b>:   ${requestScope.summary}<br />
            <b>The design has been checked</b> :   ${requestScope.design}<br />
            <b>The file is   </b>              :   ${requestScope.msa}
            <hr />
        </div>

        <div>
            <p>
                <jsp:include page="includes/ProbeResults.jsp"></jsp:include>
            </p>
        </div>

        <div>
            <p>
                <jsp:include page="includes/Footer.jsp"></jsp:include>
            </p>
        </div>

    </div>

</body>
</html>
