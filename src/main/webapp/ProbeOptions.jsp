<%--
  Created by IntelliJ IDEA.
  User: rkooistra
  Date: 27-2-18
  Time: 17:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <title>Probe Options</title>
</head>
<body>

    <div class="container">

        <div>
            <p>
                <jsp:include page="includes/Banner.jsp"></jsp:include>
            </p>
        </div>

        <div>
            <p>
            <h1>
                Probe Options
            </h1>
        </div>

        <div>
            <p>
                <jsp:include page="includes/Explanation.jsp"></jsp:include>
            </p>
        </div>

        <div>
            <p>
            <form action="Probes.do" method="post">
                The following file has been uploaded:
                <input type="text" name="MSA" value="${requestScope.MSAFileName}">

                <table class="table table-sm">
                    <tbody>
                    <tr>
                        <th scope="row"><input type="checkbox" name="MinimumProbeLength"></th>
                        <td>Minimum probe length</td>
                        <td><input type="number" name="minimum" min="5" value="12"></td>
                    </tr>
                    <tr>
                        <th scope="row"><input type="checkbox" name="MaximumProbeLength"></th>
                        <td>Maximum probe length</td>
                        <td><input type="number" name="maximum" min="20" value="27"></td>
                    </tr>
                    <tr>
                        <th scope="row"><input type="checkbox" name="NumberOfProbes"></th>
                        <td>Number of probes</td>
                        <td><input type="number" name="number" min="2" max="11" value="5"></td>
                    </tr>
                    <tr>
                        <th scope="row"><input type="checkbox" name="summary" checked></th>
                        <td>Summary of ${requestScope.MSAFileName}</td>
                    </tr>
                    <tr>
                        <th scope="row"><input type="checkbox" name="design" ></th>
                        <td>Design with default settings</td>
                    </tr>
                    </tbody>
                </table>

                <input type="submit" value="Create">
            </form>
        </div>


        <jsp:include page="includes/Footer.jsp"></jsp:include>
    </div
</body>
</html>
