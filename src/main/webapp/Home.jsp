<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: rkooistra
  Date: 26-2-18
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <title>Probe Designer</title>
</head>

<body>

    <div class="container">
        <div>
            <div>
                <div>
                    <jsp:include page="includes/Banner.jsp"></jsp:include>
                </div>
            </div>
        </div>
        <div>
            <div>
                <div>
                    <p>
                        <h1>
                            Probe Design
                        </h1>
                </div>
            </div>
        </div>

        <div>
            <div>
                <div>
                    <p>
                    <h3>Description</h3>
                    This site is used to search for the best possible probe, using the parameters the users selects. <br />
                    The probe is generated from the file the user uploads on the following page.<br />
                    This site uses a session setting so that the user doesn't have to upload the same file
                </div>
            </div>
        </div>

        <div>
            <div>
                <div>
                    <p>
                    <h3>How to use</h3>
                    To use this site, the use is first asked to upload a <i>Multi Sequence Alignment file</i>.<br />
                    After the MSA file has been uploaded the user can select different options that he wants to use. <br />
                </div>
            </div>
        </div>

        <div>
            <div>
                <div>
                    <p>
                    <h3>Options</h3>
                    Below is a short description of the various options that can be used. <br />
                    <jsp:include page="includes/Explanation.jsp"></jsp:include>
                </div>
            </div>
        </div>

        <div>
            <div>
                <div>
                    <p>
                        <a href="<c:url value="/Home.do"/>" class="btn-primary btn-sm" >Start creating probes </a>
                </div>
            </div>
        </div>

        <div>
            <div>
                <div>
                    <p>
                        <jsp:include page="includes/Footer.jsp"></jsp:include>
                    </p>
                </div>
            </div>
        </div>

    </div>
</body>

</html>
