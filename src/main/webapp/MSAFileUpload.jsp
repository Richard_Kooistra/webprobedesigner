<%--
  Created by IntelliJ IDEA.
  User: rkooistra
  Date: 26-2-18
  Time: 17:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="js/lib/FileUploadChecker.js"></script>
    <title>Probe Builder</title>
</head>
<body>

    <div class="container">

        <div>
            <p>
                <jsp:include page="includes/Banner.jsp"></jsp:include>
            </p>
        </div>

        <div>
            <p>
            <h1>
                Multi Sequence Alignment
            </h1>
        </div>

        <div>
            <p>
                The user can upload a MSA file below.<br />
                If you don't have a MSA file of your own, you can use the data file provided.<br />
            </p>
        </div>

        <div>
            <div class="form-group">
                <form method="post" action="MSA.do"
                      enctype="multipart/form-data">
                    Select file to upload: <br />
                    <input type="file" name="file" size="60" /><br />
                    <br /> <input type="submit" value="Upload" hidden/>
                </form>
            </div>
        </div>

        <div>
            <P>
                <jsp:include page="includes/Footer.jsp"></jsp:include>
            </P>
        </div>

    </div>
</body>
</html>
