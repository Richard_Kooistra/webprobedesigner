$(document).ready(
    function(){
        $('input:submit').attr('hidden',true);
        $('input:file').change(
            function(){
                if ($(this).val()){
                    $('input:submit').removeAttr('hidden');
                }
                else {
                    $('input:submit').attr('hidden',true);
                }
            });
    });