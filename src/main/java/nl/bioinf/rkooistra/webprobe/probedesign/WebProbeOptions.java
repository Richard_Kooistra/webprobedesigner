package nl.bioinf.rkooistra.webprobe.probedesign;

import nl.bioinf.rkooistra.probedesigner.ProbeMaker;
import nl.bioinf.rkooistra.probedesigner.ProbeOptionsProvider;
import nl.bioinf.rkooistra.probedesigner.SequenceCollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class WebProbeOptions {

    public static String [] getDesign(SequenceCollection sequenceCollection) {
        HashMap<String, String> sequences = sequenceCollection.getCollection();
        List<String> seqValues = new ArrayList<>(sequences.values());
        String lcd = ProbeMaker.conservedSequence(sequenceCollection);
        String sequence = seqValues.get(0);
        String start = String.valueOf(sequence.indexOf(lcd));
        String length = String.valueOf(lcd.length());
        return new String[]{start, length, lcd};
    }


    public static String getMinimumProbeLength(String s, int i) {
        return s.substring(0, i);
    }


    public static String getMaximumProbeLength(String s, int i) {
        return s.substring(0, i);
    }


    public static ArrayList<String> getNumberOfProbes(String s, int i) {
        return setNumber_of_probes(s, i);
    }


    public static LinkedHashMap<String, String> getProbeSummary(String fileName, SequenceCollection sequenceCollection) {
        LinkedHashMap<String, String> summary = ProbeMaker.summary(sequenceCollection);
        summary.replace("Filename", " ", fileName);
        return summary;
    }

    private static ArrayList<String> setNumber_of_probes(String lcd, int num) {
        ArrayList<String> number_of_probes = new ArrayList<>();
        String probes = lcd.substring(0, (lcd.length() - (lcd.length() % num)));
        int chunk = probes.length() / num;
        number_of_probes = number_of_probes;
        while (probes.length() > 0 ) {
            String probe = probes.substring(0, chunk);
            number_of_probes.add(probe);
            probes = probes.substring(chunk, probes.length());
        }
        return number_of_probes;
    }

}
