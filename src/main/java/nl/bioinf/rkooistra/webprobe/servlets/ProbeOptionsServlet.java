package nl.bioinf.rkooistra.webprobe.servlets;

import nl.bioinf.rkooistra.probedesigner.ProbeDataSource;
import nl.bioinf.rkooistra.probedesigner.ProbeFileSourceHandler;
import nl.bioinf.rkooistra.probedesigner.ProbeMaker;
import nl.bioinf.rkooistra.probedesigner.SequenceCollection;
import nl.bioinf.rkooistra.webprobe.probedesign.WebProbeOptions;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@WebServlet(name = "ProbeOptionsServlet", urlPatterns = "/Probes.do")
public class ProbeOptionsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String fileName = request.getParameter("MSA");
        request.setAttribute("msa", fileName);
        String msaFile = getServletContext().getInitParameter("upload.location") + fileName;

        ProbeDataSource probeDataSource = new ProbeFileSourceHandler(msaFile);
        SequenceCollection sequenceCollection = probeDataSource.getSequencesCollection();
        String probeSequence = ProbeMaker.conservedSequence(sequenceCollection);


        if (!(request.getParameter("MinimumProbeLength") == null )) {
            Integer minimum_length = Integer.valueOf(request.getParameter("minimum"));
            request.setAttribute("minimum", minimum_length);
            String probe = WebProbeOptions.getMinimumProbeLength(probeSequence,minimum_length);
            request.setAttribute("MinimumProbeLength", probe);
        } else request.setAttribute("minimum", " - ");

        if (!(request.getParameter("MaximumProbeLength") == null )) {
            Integer maximum_length = Integer.valueOf(request.getParameter("maximum"));
            request.setAttribute("maximum", maximum_length);
            String probe = WebProbeOptions.getMaximumProbeLength(probeSequence, maximum_length);
            request.setAttribute("MaximumProbeLength", probe);
        } else request.setAttribute("maximum", " - ");

        if (! (request.getParameter("NumberOfProbes") == null )) {
            Integer number_probes = Integer.valueOf(request.getParameter("number"));
            request.setAttribute("number", number_probes);
            ArrayList<String> NumberOfProbes = WebProbeOptions.getNumberOfProbes(probeSequence, number_probes);
            request.setAttribute("NumberOfProbes", NumberOfProbes);
        } else  request.setAttribute("number", " - ");

        if (! (request.getParameter("summary") == null )) {
            String Summary = " Checked ";
            request.setAttribute("summary", Summary);
            LinkedHashMap<String, String> msaFileSummary = WebProbeOptions.getProbeSummary(fileName, sequenceCollection);
            request.setAttribute("msaFileSummary", msaFileSummary);
        } else request.setAttribute("summary", " - ");

        if (! (request.getParameter("design") == null )) {
            String Design = " Checked ";
            request.setAttribute("design", Design);
            String[] designProbe = WebProbeOptions.getDesign(sequenceCollection);
            request.setAttribute("designStart", designProbe[0]);
            request.setAttribute("designLength", designProbe[1]);
            request.setAttribute("designSequence", designProbe[2]);
        } else request.setAttribute("design", " - ");

        RequestDispatcher view = request.getRequestDispatcher("ProbeResults.jsp");
        view.forward(request, response);

    }
}
