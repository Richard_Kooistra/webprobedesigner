package nl.bioinf.rkooistra.webprobe.servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig(fileSizeThreshold = 1024*1024*2,
        maxFileSize = 1024*1024*10,
        maxRequestSize = 1024*1024*50)
@WebServlet(name = "MSAUploadServlet", urlPatterns = "/MSA.do")
public class MSAUploadServlet extends HttpServlet {

    private String MSAFileName;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

        File savedPathOfMSAFile = new File(getServletContext().getInitParameter("upload.location"));

        for (Part part : request.getParts()) {
            String fileName = extractFileName(part);
            fileName = new File(fileName).getName();
            MSAFileName = part.getSubmittedFileName();
            File msaFile = new File(savedPathOfMSAFile, MSAFileName);
            try (InputStream msaInput = part.getInputStream()) {
                Files.copy(msaInput, msaFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }

        request.setAttribute("MSAFileName", MSAFileName);

        getServletContext().getRequestDispatcher("/ProbeOptions.jsp").forward(
                request, response);
    }


    private String extractFileName(Part part) {
        String contentDisposition = part.getHeader("content-disposition");
        String[] contentElements = contentDisposition.split(";");
        for (String contentElement : contentElements) {
            if (contentElement.trim().startsWith("filename")) {
                return contentElement.substring(contentElement.indexOf("=") + 2, contentElement.length()-1);
            }
        }
        return "";
    }

}
