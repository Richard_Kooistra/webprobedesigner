# Web Interface Probedesigner


### Description

This web application was created to provide a web interface for the Probedesigner jar created earlier this year.
<br />
This application uses the before mentioned jar file as backend to function.


### How to Use 

There are two ways to use this program, one is throught the war file that van be found in the download section, 
the other is by running the project using your favorite jave interperter.

#### War file 

* Download the war file provided in the download section.
* Copy the downloaded war file to the <i>Tomcat/webapps/</i> folder.
* Start Tomcat using <i>Tomcat/bin/startup.sh</i>
* Open your favorite Internet browser exp Chromium. 
* Go to <i>http://localhost:8080/Webprobedesigner-1.0-SNAPSHOT/Home.jsp</i>.


#### Java interpertur

* Run the project using a java interperter ex Intelij
* Run the Home.jsp file


For both options a MSA file is needed, this can be found in the download section.
